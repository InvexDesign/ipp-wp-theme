<?php /* Template Name: Get Involved */

// $args = [
//     'post_type'             => 'product',
//     'post_status'           => 'publish',
//     'ignore_sticky_posts'   => 1,
//     'posts_per_page'        => '-1',
//     'tax_query'             => array(
//         array(
//             'taxonomy'      => 'product_cat',
//             'field' => 'term_id', //This is optional, as it defaults to 'term_id'
//             'terms'         => 26,
//             'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
//         ),
//         array(
//             'taxonomy'      => 'product_visibility',
//             'field'         => 'slug',
//             'terms'         => 'exclude-from-catalog', // Possibly 'exclude-from-search' too
//             'operator'      => 'NOT IN'
//         )
//     )
// ];
// $products = new WP_Query($args);
$subscriptions = wc_get_products([
    'category' => ['memberships'],
]);

get_header(); ?>

<div class="pagemeat">
	<div class="page">
	    <?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	    
	        <div class="the-content">
    			<?php the_content(); ?>
    			
                <div class="clearer"></div>
            </div>
            
            <div id="leftcolumn">
                <h3>Online Membership Form</h3>
                <ul class="subscriptions">
                    <?php foreach($subscriptions as $subscription) : ?>
                        <li class="subscription">
                            <a href="<?php echo $subscription->add_to_cart_url(); ?>">
                                <span class="left">
                                    <span class="name"><?php echo $subscription->get_name(); ?></span>
                                    <span class="price">
                                        <?php echo $subscription->get_price_html(); ?>
                                    </span>
                                </span>
                                <span class="button">Become A Member</span>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
            <div id="rightcolumn">
				<h3>Mail-In Membership Form</h3>
				<p><em>If you are more comfortable joining by mail, a printable mail-in membership application form is available in PDF format.</em></p>
				<p style="margin-left: -7px;"><a href="/pdf/IPPc-Membership-Application-Updated.pdf"><img src="/images/button-join-pdf.png" width="300" height="52" alt="Membership Application (PDF)"></a></p>
				<p>Mail your completed membership form along with payment to the following address:</p>
				<p style="margin-left: 10px;"><strong>Illinois Prairie Path</strong><br>
				<strong>not-for-profit corporation</strong><br>
				<em>c/o: membership</em><br>
				PO Box 1086<br>
				Wheaton, IL 60187</p>
			</div>
            <div class="clearer"></div>
            
            <br />
            <br />
            
            <div class="the-content">
                
                <h4>Want to make a one time donation instead? Click <a href="#">here</a>!</h4>
            </div>
            
		<?php endwhile; endif; ?>
	</div>
</div>

<?php get_footer();
