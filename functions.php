<?php

/* Disable Editor */
define( 'DISALLOW_FILE_EDIT', true );

function getMetaSlides($metaslider_id)
{
	$args = [
		'force_no_custom_order' => true,
		'orderby'               => 'menu_order',
		'order'                 => 'ASC',
		'post_type'             => ['attachment', 'ml-slide'],
		'post_status'           => ['inherit', 'publish'],
		'lang'                  => '', // polylang, ingore language filter
		'suppress_filters'      => 1, // wpml, ignore language filter
		'posts_per_page'        => -1,
		'tax_query'             => [
			[
				'taxonomy' => 'ml-slider',
				'field'    => 'slug',
				'terms'    => $metaslider_id
			]
		]
	];

	$slides = [];
	$args = apply_filters('metaslider_populate_slides_args', $args, $metaslider_id, []);
	$query = new WP_Query($args);
	while($query->have_posts())
	{
		$query->next_post();
		$post = $query->post;
		$featured_image = get_the_post_thumbnail_url($post->ID);
		$slides[] = [
			'image_url' => $featured_image ?: $post->guid,
			'title'     => get_post_meta($post->ID, 'ml-slider_title', true),
			'content'   => $post->post_excerpt,
			'link'      => get_post_meta($post->ID, 'ml-slider_url', true),
			'button'    => get_post_meta($post->ID, '_wp_attachment_image_alt', true)
		];
	}
	wp_reset_postdata();

	return $slides;
}

function only_allow_one_item_in_cart($passed, $added_product_id) {
   wc_empty_cart();
   return $passed;
}
add_filter('woocommerce_add_to_cart_validation', 'only_allow_one_item_in_cart', 99, 2);

if(function_exists('acf_add_options_page'))
{
	acf_add_options_page(array(
		'page_title' 	=> 'Global Settings',
		'menu_title'	=> 'Global Settings',
		'menu_slug' 	=> 'global-settings',
		'capability'	=> 'edit_posts'
	));
}

function isCustomerExistingMember($order_id) {
    return get_post_meta($order_id, '_additional_wooccm1', true) === 'Yes';
}

function change_admin_new_order_email_subject($subject, $order)
{
    if(isCustomerExistingMember($order->get_id())) {
        $subject .= ' - Existing Member';
    } else {
        $subject .= ' - New Member';
    }
	
	return $subject;
}
add_filter('woocommerce_email_subject_new_order', 'change_admin_new_order_email_subject', 1, 2);
