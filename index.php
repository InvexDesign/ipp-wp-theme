<?php get_header(); ?>

<?php $slides = getMetaSlides(1054); ?>
<div class="feature" id="rotator" style="position: relative;">
	<?php foreach($slides as $slide) : ?>
		<a href="<?php echo $slide['link']; ?>"><img src="<?php echo $slide['image_url']; ?>"></a>
	<?php endforeach; ?>
</div>
<div class="pagemeat">
	<div class="page" style="overflow:none;">
		<div class="content">
			<!--<h2>News &amp; Updates</h2>
			<p>Please come to the&nbsp;<a href="https://ipp.org/wp-content/uploads/IPP-2017-annual-meeting-flyer.pdf"><em><strong>IPP Annual meeting</strong></em></a>&nbsp;Nov 5!</p>
			<p>The&nbsp;<em><strong><a href="https://ipp.org/wp-content/uploads/IPP-2017-September-Newsletter.pdf">September newsletter</a></strong></em>&nbsp;is now available.</p>
			<p>There are many ways to get involved. &nbsp;See our <em><strong><a href="https://ipp.org/events/">Upcoming Events</a></strong></em>.</p>
			<p>Please review important&nbsp;<em><strong><a href="https://ipp.org/wp-content/uploads/IPP-Safety-Tips.pdf">Safety Tips</a></strong></em></p>
			<h2>&nbsp;</h2>
			<p>&nbsp;</p>
			<h2>Our Mission</h2>
			<p>To preserve, enhance, and advocate on behalf of the Illinois Prairie Path for current and future generations.</p>
			<h2>&nbsp;</h2>
			<h2>Check Out our Videos</h2>
			<p><em><a href="https://youtu.be/ZIwABHwSUc4" target="_blank" rel="noopener noreferrer">Highlights Video (1:02)</a></em><br>
				<em><a href="https://youtu.be/vJEt0RprCdE" target="_blank" rel="noopener noreferrer">Intro to the Illinois Prairie Path (8:04)</a></em></p>-->
            <?php
                $content = '';
                $page = get_page_by_title('Home');
                $my_id = $page->ID;
                $post_id_1 = get_post($my_id);
                $content = $post_id_1->post_content;
                $content = apply_filters('the_content', $content);
                $content = str_replace(']]>', ']]>', $content);
                echo $content;
            ?>
		</div>
		<div class="column">
			<h2>Getting Involved</h2>
			<p>We invite you to become a member and to help make this one of the best trail systems not only in Illinois, but in the United States and throughout the world. Together, we will promote the health and well-being of our trail, our environment and our communities.</p>
			<ul>
				<li id="join"><a href="https://ipp.org/gettinginvolved" title="Become a member and enjoy exclusive membership benefits">Become a member and enjoy exclusive membership benefits</a></li>
				<li id="donate"><a href="https://ipp.org/donate/" title="Make a donation and help fund improvement projects">Make a donation and help fund improvement projects</a></li>
				<li id="contact"><a href="https://ipp.org/contact-us" title="Volunteer info, membership info &amp; more">Volunteer info, membership info &amp; more</a></li>
			</ul>
		</div>
		<div style="clear:both;height:1px;line-height:1px;"> </div>
	</div>
</div>

<?php get_footer(); ?>

