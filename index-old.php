<html>
<head>
	<title>Illinois Prairie Path. North America's first successful rails-to-trails conversion. Founded 1963.</title>
	<link rel="stylesheet" href="/css/presets.css" type="text/css" media="screen">
	<link rel="stylesheet" href="/css/ipp.css" type="text/css" media="screen">
	<link rel="stylesheet" href="/css/pages.css" type="text/css" media="screen">
	<style type="text/css">
			table.tableizer-table {
			border: 1px solid #CCC; font-family: Arial, Helvetica, sans-serif
				font-size: 12px;
			}
			th, td {
				border: 1px solid #CCC;
				font-size: 14px;
			}
			#brian-ipp-table {
				font-family:Arial, Helvetica, sans-serif;	
				border:none;
			}
			#brian-ipp-table td {
				padding-right:25px;
				border:none;
			}
			#brian-ipp-table h4 {
				color:#333;
				font-size:14px;
				margin:0 0 5px 0;
				padding:0;
			}
			#brian-ipp-table ul {
				list-style:none;
				padding:0;
				margin:0 0 5px 5px;	
			}
			#brian-ipp-table ul li {
				margin:0 0 4px 0;
				padding:0;
				line-height:18px;	
			}
			#brian-ipp-table ul li a:link, #brian-ipp-table ul li a:visited {
				color:#030;
				text-decoration:none;
				font-size:13px;
			}
			#brian-ipp-table ul li a:hover, #brian-ipp-table ul li a:active {
				text-decoration:underline;
			}
		</style>
		<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAa1mPVnRbptzW-c0PLHCWRciWoWbqPq8c"></script>
		<script src="map-includes/GXml.js" type="text/javascript"></script>
		<script src="map-includes/map.js" type="text/javascript"></script>
</head>

<body id="page">

<div class="container">
	<div class="masthead">
<?php include '../includes/logo.php'; ?>
<?php include '../includes/social.php'; ?>
	</div>
<?php include '../includes/navbar.php'; ?>
	<div class="pagemeat">
		<div class="page">
			<h2>Illinois Prairie Path Trail Map</h2>
            <div id="map_canvas" style="width:840px; height:600px; margin-bottom:25px;"></div>
			<table id="brian-ipp-table" cellpadding="0" cellspacing="0">
            	<tr>
                	<td valign="top" width="30%">
                    	<h4>Main Branch</h4>
                        <ul>
                        	<li><a id="43" href="#map_canvas">Maywood Trailhead</a></li>
                            <li><a id="44" href="#map_canvas">Berkeley Town Park</a></li>
                            <li><a id="45" href="#map_canvas">Wild Meadow Trace Park</a></li>
                            <li><a id="46" href="#map_canvas">Spring Rd</a></li>
                            <li><a id="47" href="#map_canvas">Villa Park Historical Society Museum</a></li>
                            <li><a id="48" href="#map_canvas">Ardmore Ave</a></li>
                            <li><a id="49" href="#map_canvas">Harvard Ave</a></li>
                            <li><a id="50" href="#map_canvas">Westmore Ave</a></li>
                        </ul>
                    </td>
                    <td valign="top" width="20%">
                    	<h4>&nbsp;</h4>
                        <ul>
                        	<li><a id="51" href="#map_canvas">Grace St</a></li>
                            <li><a id="52" href="#map_canvas">Main St</a></li>
                            <li><a id="53" href="#map_canvas">Walnut Glen Park</a></li>
                            <li><a id="54" href="#map_canvas">Prairie Path Park</a></li>
                            <li><a id="55" href="#map_canvas">Hoffman Park</a></li>
                            <li><a id="56" href="#map_canvas">Wheaton College</a></li>
                            <li><a id="57" href="#map_canvas">Founders Park</a></li>
                        </ul>
                    </td>
                    <td valign="top" width="25%">
                    	<h4>Elgin Branch</h4>
                        <ul>
                        	<li><a id="58" href="#map_canvas">Volunteer Park</a></li>
                            <li><a id="65" href="#map_canvas">Lincoln Marsh Wetlands</a></li>
                            <li><a id="66" href="#map_canvas">Geneva &amp; County Farm Rd</a></li>
                            <li><a id="67" href="#map_canvas">Kline Creek Farm</a></li>
                            <li><a id="68" href="#map_canvas">St. Andrews Country Club</a></li>
                            <li><a id="69" href="#map_canvas">Army Trail Rd</a></li>
                            <li><a id="70" href="#map_canvas">Valley Model Railroad HQ</a></li>
                            <li><a id="71" href="#map_canvas">South Elgin Trailhead</a></li>
                        </ul>
                    </td>
                    <td valign="top" width="25%">
                    	<h4>Aurora Branch</h4>
                        <ul>
                        	<li><a id="59" href="#map_canvas">Prairie Path Park</a></li>
                            <li><a id="60" href="#map_canvas">St. James Farm</a></li>
                            <li><a id="61" href="#map_canvas">Warrenville Town Park</a></li>
                            <li><a id="62" href="#map_canvas">Diehl &amp; Shore Rd</a></li>
                            <li><a id="63" href="#map_canvas">Fox River Path</a></li>
                        </ul>
                        <h4>Batavia Spur</h4>
                        <ul>
                        	<li><a id="64" href="#map_canvas">Glenwood Park</a></li>
                        </ul>
                        <h4>Geneva Spur</h4>
                        <ul>
                        	<li><a id="72" href="#map_canvas">Reed Keppler Park</a></li>
                        </ul>
                    </td>
                </tr>
            </table>
            <hr />
			<div class="photo" id="right"><img src="/images/pic-member-map.jpg" width="340" height="260" alt="IPPc Member Map"></div>
			<p>A full map of the Illinois Prairie Path is available in two versions, one basic map in color, and one black &amp; white. Both are made available online in PDF format.</p>
			<ul>
				<li><a href="/pdf/IPP-free-map-bw-updated.pdf">Illinois Prairie Path - Trail Map</a> (Black &amp; White)</li>
				<li><a href="/pdf/IPP-free-map-color-updated.pdf">Illinois Prairie Path - Trail Map</a> (Basic Color)</li>
			</ul>
			<p><a href="/gettinginvolved/"><strong>Become a member of the Illinois Prairie Path corporation today and receive your official full-color trail map along with your membership.</strong></a></p>
            <p>The member map is updated periodically and is now in its seventh edition. To become a member, please visit the <em><a href="http://ipp.org/gettinginvolved/">Membership page</a></em>.</p>
			<p>This full color and full detail map can be purchased at <em><a href="http://www.midwestcyclery.com/" target="_blank">Midwest Cyclery</a></em> in downtown Wheaton.</p>
			<p>PARKING can be found on the <em><a href="https://www.dupageco.org/EDP/Bikeways_and_Trails/Docs/18299/" target="_blank">Dupage County map</a></em>.</p>
            <div id="user_selection_table_div" style="display:none;">
				<table id="user_selection_table">
					<tr>
						<td width="293px">
							<table id="main_branch_table" class="tableizer-table">
								<tr>
									<th>Main Branch Markers</th>
								</tr>
							</table>
						</td>
						<td width="293px">
							<table id="elgin_branch_table" class="tableizer-table">
								<tr>
									<th>Elgin Branch Markers</th>
								</tr>
							</table>
						</td>
						<td width="293px">
							<table id="aurora_branch_table" class="tableizer-table">
								<tr>
									<th>Aurora Branch Markers</th>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
            
		</div>
	</div>
<?php include '../includes/buttons.php'; ?>
<?php include '../includes/footer.php'; ?>
</div>
<?php include '../includes/google.php'; ?>
</body>
</html>