<?php /* Template Name: Trail Map */ ?>

<?php get_header(); ?>

	<link rel="stylesheet" href="https://ipp.org/css/presets.css" type="text/css" media="screen">
<!--	<link rel="stylesheet" href="https://ipp.org/css/ipp.css" type="text/css" media="screen">-->
	<link rel="stylesheet" href="https://ipp.org/css/pages.css" type="text/css" media="screen">
	<style type="text/css">
		.branch-table {
			font-family:Arial, Helvetica, sans-serif;
			border:none;
			display: flex;
		}
		.branch-table .col {
			padding-right:25px;
			flex: 1;
		}
		.branch-table h4 {
			color:#333;
			font-size:14px;
			margin:0 0 5px 0;
			padding:0;
		}
		.branch-table ul {
			list-style:none;
			padding:0;
			margin:0 0 5px 5px;
		}
		.branch-table ul li {
			margin:0 0 4px 0;
			padding:0;
			line-height:18px;
		}
		.branch-table ul li a:link, .branch-table ul li a:visited {
			color:#030;
			text-decoration:none;
			font-size:13px;
		}
		.branch-table ul li a:hover, .branch-table ul li a:active {
			text-decoration:underline;
		}
		.photo.right {
			float: right;
			margin: 0 0 10px 10px;
		}
	</style>
	
	<?php /* OLD KEY: AIzaSyBvYGrTpUpssH0McRxlCEyTJHB-BSbHjjI */ ?>
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSWcAQklsLz_wPevDOSLk7RZ35IlaL38Y"></script>
	<?php /* GXml.js truly isnt present, what was this for?  */ ?>
	<!--<script src="https://ipp.org/trail-maps/map-includes/GXml.js" type="text/javascript"></script>-->
	<script src="https://ipp.org/trail-maps/map-includes/map.js" type="text/javascript"></script>

	<div class="pagemeat map">
		<div class="page">
		<?php if(have_posts()) : ?>
		<?php while(have_posts()) : the_post(); ?>
			<h2>Illinois Prairie Path Trail Map</h2>
			<div id="map_canvas" style="width: 100% !important; max-width:840px; height:600px; margin-bottom:25px;"></div>
			<div class="branch-table">
				<div class="col one">
					<h4>Main Branch</h4>
					<ul>
						<li><a id="43" href="#map_canvas">Maywood Trailhead</a></li>
						<li><a id="44" href="#map_canvas">Berkeley Town Park</a></li>
						<li><a id="45" href="#map_canvas">Wild Meadow Trace Park</a></li>
						<li><a id="46" href="#map_canvas">Spring Rd</a></li>
						<li><a id="47" href="#map_canvas">Villa Park Historical Society Museum</a></li>
						<li><a id="48" href="#map_canvas">Ardmore Ave</a></li>
						<li><a id="49" href="#map_canvas">Harvard Ave</a></li>
						<li><a id="50" href="#map_canvas">Westmore Ave</a></li>
					</ul>
				</div>
				<div class="col two">
					<h4 id="blank_h4">&nbsp;</h4>
					<ul>
						<li><a id="51" href="#map_canvas">Grace St</a></li>
						<li><a id="52" href="#map_canvas">Main St</a></li>
						<li><a id="53" href="#map_canvas">Walnut Glen Park</a></li>
						<li><a id="54" href="#map_canvas">Prairie Path Park</a></li>
						<li><a id="55" href="#map_canvas">Hoffman Park</a></li>
						<li><a id="56" href="#map_canvas">Wheaton College</a></li>
						<li><a id="57" href="#map_canvas">Founders Park</a></li>
					</ul>
				</div>
				<div class="col three">
					<h4>Elgin Branch</h4>
					<ul>
						<li><a id="58" href="#map_canvas">Volunteer Park</a></li>
						<li><a id="65" href="#map_canvas">Lincoln Marsh Wetlands</a></li>
						<li><a id="66" href="#map_canvas">Geneva &amp; County Farm Rd</a></li>
						<li><a id="67" href="#map_canvas">Kline Creek Farm</a></li>
						<li><a id="68" href="#map_canvas">St. Andrews Country Club</a></li>
						<li><a id="69" href="#map_canvas">Army Trail Rd</a></li>
						<li><a id="70" href="#map_canvas">Valley Model Railroad HQ</a></li>
						<li><a id="71" href="#map_canvas">South Elgin Trailhead</a></li>
					</ul>
				</div>
				<div class="col four">
					<h4>Aurora Branch</h4>
					<ul>
						<li><a id="59" href="#map_canvas">Prairie Path Park</a></li>
						<li><a id="60" href="#map_canvas">St. James Farm</a></li>
						<li><a id="61" href="#map_canvas">Warrenville Town Park</a></li>
						<li><a id="62" href="#map_canvas">Diehl &amp; Shore Rd</a></li>
						<li><a id="63" href="#map_canvas">Fox River Path</a></li>
					</ul>
					<h4>Batavia Spur</h4>
					<ul>
						<li><a id="64" href="#map_canvas">Glenwood Park</a></li>
					</ul>
					<h4>Geneva Spur</h4>
					<ul>
						<li><a id="72" href="#map_canvas">Reed Keppler Park</a></li>
					</ul>
				</div>
			</div>
			<hr />
			<div class="photo right"><img src="https://ipp.org/images/pic-member-map.jpg" width="340" height="260" alt="IPPc Member Map"></div>
			<p>A full map of the Illinois Prairie Path is available in two versions, one basic map in color, and one black &amp; white. Both are made available online in PDF format.</p>
			<ul>
				<li><a href="https://ipp.org/pdf/IPP-free-map-bw-updated.pdf">Illinois Prairie Path - Trail Map</a> (Black &amp; White)</li>
				<li><a href="https://ipp.org/pdf/IPP-free-map-color-updated.pdf">Illinois Prairie Path - Trail Map</a> (Basic Color)</li>
			</ul>
			<p><a href="https://ipp.org/gettinginvolved/"><strong>Become a member of the Illinois Prairie Path corporation today and receive your official full-color trail map along with your membership.</strong></a></p>
			<p>The member map is updated periodically and is now in its seventh edition. To become a member, please visit the <em><a href="http://ipp.org/gettinginvolved/">Membership page</a></em>.</p>
			<p>This full color and full detail map can be purchased at <em><a href="http://www.midwestcyclery.com/" target="_blank">Midwest Cyclery</a></em> in downtown Wheaton.</p>
			<p>PARKING can be found on the <em><a href="https://www.dupageco.org/EDP/Bikeways_and_Trails/Docs/18299/" target="_blank">Dupage County map</a></em>.</p>
			<div id="user_selection_table_div" style="display:none;">
				<table id="user_selection_table">
					<tr>
						<td width="293px">
							<table id="main_branch_table" class="tableizer-table">
								<tr>
									<th>Main Branch Markers</th>
								</tr>
							</table>
						</td>
						<td width="293px">
							<table id="elgin_branch_table" class="tableizer-table">
								<tr>
									<th>Elgin Branch Markers</th>
								</tr>
							</table>
						</td>
						<td width="293px">
							<table id="aurora_branch_table" class="tableizer-table">
								<tr>
									<th>Aurora Branch Markers</th>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</div>
			<div class="clearer"> </div>
		<?php endwhile; ?>
		<?php endif; ?>
		</div>
	</div>

<?php get_footer();
